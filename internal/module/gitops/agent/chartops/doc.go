package chartops

//go:generate go run github.com/golang/mock/mockgen -self_package "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v15/internal/module/gitops/agent/chartops" -destination "mock_for_test.go" -package "chartops" "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v15/internal/module/gitops/agent/chartops" "Helm"
